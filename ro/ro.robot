*** Settings ***
Documentation     A test suite with a single test for Google
...               Created by hats' Robotcorder
Library           SeleniumLibrary    timeout=10

*** Variables ***
${BROWSER}    headlesschrome
${SLEEP}    3

*** Test Cases ***
Google test
    Open Browser    https://www.google.com/    ${BROWSER}
    Input Text    //input[@name="q"]    robot

    Close Browser